<?php

function add_user_experience($request)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "experiences";
    $user_id = get_current_user_id();

    if (isset($request['job'])) {
        $job = sanitize_text_field($request['job']);
        $place = sanitize_text_field($request['place ']);
        $date_in = strtotime(sanitize_text_field($request['date_in']));
        $date_in = date('Y-m-d', $date_in);
        $date_out = strtotime(sanitize_text_field($request['date_out']));
        $date_out = date('Y-m-d', $date_out);
        if (!empty($experience)) {
            $wpdb->insert(
                $table_name,
                array(
                    'user_id' => 1,
                    'job' => $job,
                    'place ' => $place ,
                    'date_in' => $date_in,
                    'date_out' => $date_out
                )
            );
            wp_send_json_success();
        } else {
            wp_send_json_error();
        }
    } else {
        wp_send_json_error();
    }
}

add_action('rest_api_init', function () {
    register_rest_route('my-namespace/v1', '/addexperiences', array(
        'methods' => 'POST',
        'callback' => 'add_user_experience',
    ));
});
