<?php

function add_user_formation($request)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "formation";
    $user_id = get_current_user_id();

    if (isset($request['diplome'])) {
        $diplome = sanitize_text_field($request['diplome']);
        $specialite = sanitize_text_field($request['specialite']);
        if (!empty($formation)) {
            $wpdb->insert(
                $table_name,
                array(
                    'user_id' => $user_id,
                    'diplome' => $diplome,
                    'specialite' => $specialite
                )
            );
            wp_send_json_success();
        } else {
            wp_send_json_error();
        }
    } else {
        wp_send_json_error();
    }
}

add_action('rest_api_init', function () {
    register_rest_route('my-namespace/v1', '/addformation', array(
        'methods' => 'POST',
        'callback' => 'add_user_formation',
    ));
});
