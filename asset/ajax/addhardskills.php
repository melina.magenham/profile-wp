<?php

function add_user_hardskill($request)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "hardskills";
    $user_id = get_current_user_id();

    if (isset($request['nom_competence'])) {
        $hardskill = sanitize_text_field($request['nom_competence']);
        $level = $request['niveau'];
        if (!empty($hardskill)) {
            $wpdb->insert(
                $table_name,
                array(
                    'user_id' => $user_id,
                    'nom_competence' => $hardskill,
                    'niveau' => $level
                )
            );
            wp_send_json_success();
        } else {
            wp_send_json_error();
        }
    } else {
        wp_send_json_error();
    }
}

add_action('rest_api_init', function () {
    register_rest_route('my-namespace/v1', '/addhardskills', array(
        'methods' => 'POST',
        'callback' => 'add_user_hardskill',
    ));
});
