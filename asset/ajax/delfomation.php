<?php
function delete_softskill( $request ) {
    global $wpdb;
    $table_name = $wpdb->prefix . "softskills";
    $softskill_id = intval( $request->get_param( 'id' ) );
    $wpdb->delete(
        $table_name,
        array( 'id' => $softskill_id ),
        array( '%d' )
    );
    return wp_send_json_success();
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'my-namespace/v1', '/deletesoftskills/(?P<id>\d+)', array(
        'methods' => 'DELETE',
        'callback' => 'delete_softskill',
    ));
});

