<?php
function delete_hardskill( $request ) {
    global $wpdb;
    $table_name = $wpdb->prefix . "hardskills";
    $hardskill_id = intval( $request->get_param( 'id' ) );
    $wpdb->delete(
        $table_name,
        array( 'id' => $hardskill_id ),
        array( '%d' )
    );
    return wp_send_json_success();
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'my-namespace/v1', '/deletehardskills/(?P<id>\d+)', array(
        'methods' => 'DELETE',
        'callback' => 'delete_hardskill',
    ));
});

