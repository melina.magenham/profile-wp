<?php


function get_user_formation()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "formation";
    $user_id = get_current_user_id();
    $results = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT id, diplome, specialite FROM $table_name WHERE user_id = 1"
        )
    );
    wp_send_json_success($results);
}

add_action('rest_api_init', function () {
    register_rest_route('my-namespace/v1', '/formation', array(
        'methods' => 'GET',
        'callback' => 'get_user_formation',
    ));
});


