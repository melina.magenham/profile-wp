<?php


function get_user_hardskills()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "hardskills";
    $user_id = get_current_user_id();
    $results = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT id, nom_competence, niveau FROM $table_name WHERE user_id = %d",
            $user_id
        )
    );
    if (!empty($results)) {
        wp_send_json_success($results);
    } else {
        wp_send_json_error();
    }
}

add_action('rest_api_init', function () {
    register_rest_route('my-namespace/v1', '/hardskills', array(
        'methods' => 'GET',
        'callback' => 'get_user_hardskills',
    ));
});


