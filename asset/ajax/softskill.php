<?php



function get_user_softskills()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "softskills";
    $user_id = get_current_user_id();
    $results = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT id, nom_competence FROM $table_name WHERE user_id = %d",
            $user_id
        )
    );
    if (!empty($results)) {
        wp_send_json_success($results);
    } else {
        wp_send_json_error();
    }
}

add_action('rest_api_init', function () {
    register_rest_route('my-namespace/v1', '/softskills', array(
        'methods' => 'GET',
        'callback' => 'get_user_softskills',
    ));
});


