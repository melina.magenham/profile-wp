console.log('Exo Melvin');

// MODAL
const modal = document.getElementById("modal");
const modal2 = document.getElementById("modal2");
const openModal = document.getElementById("openModal");
const openModal2 = document.getElementById("openModal2");
const openModal3 = document.getElementById("openModal3");
const close = document.getElementsByClassName("close")[0];
const close2 = document.getElementsByClassName("close2")[0];

openModal.onclick = function(evt) {
    evt.preventDefault();
    modal.style.display = "block";
    document.body.style.overflow = "hidden";
}

openModal2.onclick = function(evt) {
    evt.preventDefault();
    modal.style.display = "block";
    document.body.style.overflow = "hidden";
}

openModal3.onclick = function(evt) {
    evt.preventDefault();
    modal2.style.display = "block";
    document.body.style.overflow = "hidden";
}

close.onclick = function() {
    modal.style.display = "none";
    document.body.style.overflow = "auto";
}

close2.onclick = function() {
    modal2.style.display = "none";
    document.body.style.overflow = "auto";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        document.body.style.overflow = "auto";
    }

    if (event.target == modal2) {
        modal2.style.display = "none";
        document.body.style.overflow = "auto";
    }
}

$(document).ready(function() {
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});



