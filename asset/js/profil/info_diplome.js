////////////
// Hardskill
////////////
document.querySelector('#title-diplome').addEventListener('click', function() {
    document.querySelector('#todo-form3').style.height =
        document.querySelector('#todo-form3').style.marginTop =
            document.querySelector('#todo-form3').style.padding =
                document.querySelector('#todo-form3').style.height === 'auto' ? '0px' : 'auto';
    if (document.querySelector('#todo-form3').style.height === 'auto') {
        document.querySelector('#todo-form3').style.padding = '.5rem'
        document.querySelector('#todo-form3').style.marginTop = '1rem'
        document.querySelector('#close-diplome').innerHTML = '-'
    } else {
        document.querySelector('#close-diplome').innerHTML = '+'
        document.querySelector('#todo-form3').style.padding = ''
        document.querySelector('#todo-form3').style.marginTop = ''

    }
})

//ajax load
function getformation() {
    fetch('../wp-json/my-namespace/v1/formation')
        .then(response => response.json())
        .then(data => {
            let output = '';
            data.data.forEach(formation => {
                output += `<li>${formation.diplome} <span>${formation.specialite}</span> <span class="delete-formation-button" data-formation-id="${formation.id}">x</span></li>`;
            });
            document.querySelector('#todo-list3').innerHTML = output;
        });
}

document.addEventListener('DOMContentLoaded', getformation);

// listner addhardskill
const addhardSkillForm = document.querySelector('.add-group3');
addhardSkillForm.addEventListener('submit', addhardSkill);

function adddiplome(e) {
    e.preventDefault();

    // recupère la value de la hardskill
    const diplome = document.querySelector('#task-name2').value;
    const diplome_name = document.querySelector('#diplome_name').value;

    // envoie la nouvelle hardskill
    fetch('/wordpress/wp-json/my-namespace/v1/addformation', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            diplome: diplome,
            specialite: diplome_name
        })
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            // Add the new hard skill to the list
            const todoList = document.querySelector('#todo-list3');
            const newdiplomeItem = document.createElement('li');
            newhardSkillItem.innerHTML = `${diplome} <div><span>${diplome_name}</span> <span class="delete-softskill-button">x</span></div>`;
            todoList.appendChild(newdiplomeItem);
        });
}
