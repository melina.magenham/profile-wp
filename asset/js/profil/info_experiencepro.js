//////////////
// experiencePro
//////////////
document.querySelector('#title-experience').addEventListener('click', function() {
    document.querySelector('#div-experience').style.height =
        document.querySelector('#div-experience').style.marginTop =
            document.querySelector('#div-experience').style.padding =
                document.querySelector('#div-experience').style.height === 'auto' ? '0px' : 'auto';
    if (document.querySelector('#div-experience').style.height === 'auto') {
        document.querySelector('#div-experience').style.padding = '.5rem';
        document.querySelector('#div-experience').style.marginTop = '1rem';
        document.querySelector('#close-experience').innerHTML = '-';
    } else {
        document.querySelector('#close-experience').innerHTML = '+';
        document.querySelector('#div-experience').style.padding = '';
        document.querySelector('#div-experience').style.marginTop = '';
    }
});

//ajax

//ajax load
function getexperiences() {
    fetch('../wp-json/my-namespace/v1/experience')
        .then(response => response.json())
        .then(data => {
            let output = '';
            data.data.forEach(experience => {
                output += `<li>${experience.job}${experience.place}<div><span>${experience.date_in} - ${experience.date_out}</span> <span class="delete-experience-button" data-experience-id="${experience.id}">x</span></div></li>`;
            });
            document.querySelector('#experience-list').innerHTML = output;
        });
}

document.addEventListener('DOMContentLoaded', getexperiences);

function addexperience(e) {
    e.preventDefault();
    console.log('click');

    // Recuper la value de la experience
    const newjob = document.querySelector('#job').value;
    const newplace = document.querySelector('#where').value;
    const newdate_in = document.querySelector('#date_in').value;
    const newdate_out = document.querySelector('#date_out').value;

    // Envoie la nouvelle experience
    fetch('../wp-json/my-namespace/v1/addexperiences', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            job: newjob,
            place: newplace,
            date_in: newdate_in,
            date_out: newdate_out
        })
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            // Add the new experience to the list
            const todoList = document.querySelector('#experience-list');
            const newexperienceItem = document.createElement('li');
            newexperienceItem.innerHTML = `<li>${newjob}${newplace}<div><span>${newdate_in} - ${newdate_out}</span> <span class="delete-experience-button">x</span></div></li>`;
            todoList.appendChild(newexperienceItem);

            // Clear the form fields
            document.querySelector('#job').value = '';
            document.querySelector('#where').value = '';
            document.querySelector('#date_in').value = '';
            document.querySelector('#date_out').value = '';
        });
}

document.querySelector('form.add-group4').addEventListener('submit', addexperience);
