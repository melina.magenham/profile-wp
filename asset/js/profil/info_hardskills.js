////////////
// Hardskill
////////////
document.querySelector('#title-hardskill2').addEventListener('click', function() {
    document.querySelector('#todo-form2').style.height =
        document.querySelector('#todo-form2').style.marginTop =
            document.querySelector('#todo-form2').style.padding =
                document.querySelector('#todo-form2').style.height === 'auto' ? '0px' : 'auto';
    if (document.querySelector('#todo-form2').style.height === 'auto') {
        document.querySelector('#todo-form2').style.padding = '.5rem'
        document.querySelector('#todo-form2').style.marginTop = '1rem'
        document.querySelector('#close-hardskill2').innerHTML = '-'
    } else {
        document.querySelector('#close-hardskill2').innerHTML = '+'
        document.querySelector('#todo-form2').style.padding = ''
        document.querySelector('#todo-form2').style.marginTop = ''

    }
})
//ajax

//ajax load
function gethardSkills() {
    fetch('../wp-json/my-namespace/v1/hardskills')
        .then(response => response.json())
        .then(data => {
            let output = '';
            data.data.forEach(hardskill => {
                output += `<li>${hardskill.nom_competence}<div><span>${hardskill.niveau}</span> <span class="delete-hardskill-button" data-hardskill-id="${hardskill.id}">x</span></div></li>`;
            });
            document.querySelector('#todo-list2').innerHTML = output;
        });
}

document.addEventListener('DOMContentLoaded', gethardSkills);

// listner addhardskill
const addhardSkillForm = document.querySelector('.add-group2');
addhardSkillForm.addEventListener('submit', addhardSkill);

function addhardSkill(e) {
    e.preventDefault();

    // recupère la value de la hardskill
    const newhardSkill = document.querySelector('#task-name2').value;
    const newhardSkilllvl = document.querySelector('#rating2').value;

    // envoie la nouvelle hardskill
    fetch('../wp-json/my-namespace/v1/addhardskills', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            nom_competence: newhardSkill,
            niveau: newhardSkilllvl
        })
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            // Add the new hard skill to the list
            const todoList = document.querySelector('#todo-list2');
            const newhardSkillItem = document.createElement('li');
            newhardSkillItem.innerHTML = `${newhardSkill} <div><span>${newhardSkilllvl}</span> <span class="delete-softskill-button">x</span></div>`;
            todoList.appendChild(newhardSkillItem);
        });
}


//delete hard skill


function deletehardSkill(e) {
    e.preventDefault();

    // Récupérer l'ID de la compétence à partir de l'élément cible de l'événement
    let hardSkillId = e.target.dataset.hardskillId;
    fetch(`../wp-json/my-namespace/v1/deletehardskills/${hardSkillId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            hardSkillId: hardSkillId
        })
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            // Supprimer la hardskill de la liste
            e.target.parentElement.parentElement.remove();
        });
}

// Ajouter un écouteur d'événement sur chaque bouton de suppression
document.querySelector('#todo-list2').addEventListener('click', (event) => {
    if (event.target.matches('.delete-hardskill-button')) {
        deletehardSkill(event);
    }
});