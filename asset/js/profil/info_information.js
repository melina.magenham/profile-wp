////////////
// Information
////////////

//dropdown

document.querySelector('#title-info').addEventListener('click', function() {
    document.querySelector('#close-info').innerHTML =
        document.querySelector('#form').style.height =
            document.querySelector('#form').style.padding =
                document.querySelector('#form').style.height === '400px' ? '0px' : '400px';
    if (document.querySelector('#form').style.height === '400px') {
        document.querySelector('#form').style.padding = '1.5rem 1rem'
        document.querySelector('#close-info').innerHTML = '-'
    } else {
        document.querySelector('#close-info').innerHTML = '+'
        document.querySelector('#form').style.padding = ''
    }
});
