
////////////
// Softskill
////////////

//dropdown
document.querySelector('#title-softskill').addEventListener('click', function() {
    document.querySelector('#todo-form').style.height =
        document.querySelector('#todo-form').style.marginTop =
            document.querySelector('#todo-form').style.padding =
                document.querySelector('#todo-form').style.height === 'auto' ? '0px' : 'auto';
    if (document.querySelector('#todo-form').style.height === 'auto') {
        document.querySelector('#todo-form').style.padding = '.5rem'
        document.querySelector('#todo-form').style.marginTop = '1rem'
        document.querySelector('#close-softskill').innerHTML = '-'
    } else {
        document.querySelector('#close-softskill').innerHTML = '+'
        document.querySelector('#todo-form').style.padding = ''
        document.querySelector('#todo-form').style.marginTop = ''

    }
})

//ajax load
function getSoftSkills() {
    fetch('../wp-json/my-namespace/v1/softskills')
        .then(response => response.json())
        .then(data => {
            let output = '';
            data.data.forEach(softskill => {
                output += `<li>${softskill.nom_competence} <span class="delete-softskill-button" data-softskill-id="${softskill.id}">x</span></li>`;
            });
            document.querySelector('#todo-list').innerHTML = output;
        });
}

document.addEventListener('DOMContentLoaded', getSoftSkills);

// listner addsoftskill
const addSoftSkillForm = document.querySelector('.add-group');
addSoftSkillForm.addEventListener('submit', addSoftSkill);

function addSoftSkill(e) {
    e.preventDefault();

    // recupère la value de la softskill
    const newSoftSkill = document.querySelector('#task-name').value;

    // envoie la nouvelle softskill
    fetch('../wp-json/my-namespace/v1/addsoftskills', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            nom_competence: newSoftSkill
        })
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            // Add the new soft skill to the list
            const todoList = document.querySelector('#todo-list');
            const newSoftSkillItem = document.createElement('li');
            newSoftSkillItem.innerHTML = `${newSoftSkill} <span class="delete-softskill-button">x</span>`;
            todoList.appendChild(newSoftSkillItem);
        });
}

//delete soft skill


function deleteSoftSkill(e) {
    e.preventDefault();

    // Récupérer l'ID de la compétence à partir de l'élément cible de l'événement
    let softSkillId = e.target.dataset.softskillId;
    console.log(softSkillId)
    fetch(`../wp-json/my-namespace/v1/deletesoftskills/${softSkillId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            softSkillId: softSkillId
        })
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            // Supprimer la softskill de la liste
            e.target.parentElement.remove();
        });
}

// Ajouter un écouteur d'événement sur chaque bouton de suppression
document.querySelector('#todo-list').addEventListener('click', (event) => {
    if (event.target.matches('.delete-softskill-button')) {
        deleteSoftSkill(event);
    }
});