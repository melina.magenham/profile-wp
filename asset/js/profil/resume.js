//softskill

//ajax load
function getSoftSkills() {
    fetch('../wp-json/my-namespace/v1/softskills')
        .then(response => response.json())
        .then(data => {
            let output = '';
            data.data.forEach(softskill => {
                output += `<div class="box"><h2 class="titles">${softskill.nom_competence}</H2></div><line></line>`;
            });
            document.querySelector('#competence').innerHTML = output;
        });
}

document.addEventListener('DOMContentLoaded', getSoftSkills);

function gethardskills() {
    fetch('../wp-json/my-namespace/v1/hardskills')
        .then(response => response.json())
        .then(data => {
            let output = '';
            data.data.forEach(hardskill => {
                output += `<div class="box"><h2 class="titles">${hardskill.nom_competence}</H2><h2 class="LVL">${hardskill.niveau}/10</H2></div><line></line>`;
            });
            document.querySelector('#hardskill').innerHTML = output;
        });
}

document.addEventListener('DOMContentLoaded', gethardskills);

function getexperiences() {
    fetch('../wp-json/my-namespace/v1/experience')
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok');
        })
        .then(data => {
            console.log(data)
            let output = '';
            if (Array.isArray(data.data)) {
                data.data.forEach(experience => {
                    output += `<div class="box"><h2 class="titles">${experience.job}</H2><h2 class="LVL">${experience.place}</H2><p class="text">De ${experience.date_in} à ${experience.date_out}</p></div><line></line>`;
                });
            } else {
                console.error('data.data is not an array');
            }
            document.querySelector('#experience').innerHTML = output;
        })
}
document.addEventListener('DOMContentLoaded', getexperiences);



function getformation() {
    fetch('../wp-json/my-namespace/v1/formation')
        .then(response => response.json())
        .then(data => {
            let output = '';
            data.data.forEach(formation => {
                output += `<div class="boxline"><h2>${formation.diplome} </h2><p>${formation.specialite}</p></div><line></line>`;
            });
            document.querySelector('#resumeformation').innerHTML = output;
        });
}

document.addEventListener('DOMContentLoaded', getformation);

