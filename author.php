<?php get_header()?>

<div class="wrap2">
    <div id="profil-main2">
        <div id="sidebar_left">
            <?php get_template_part('view/recruteur/recrutement', 'sidebar'); ?>
        </div>

        <section id="infos_single_user">
            <div class="isu_box_top">
                <div class="isu_bt_left">
                    <div class="isu_btl_topleft">
                        <img src="<?= asset('/profil-linkedin-1 1.png'); ?>" alt="pdp">
                    </div>
                    <div class="isu_btl_topright">
                        <h1>John Fernandis</h1>
                        <h2>Senior Dev Ops</h2>
                    </div>
                </div>
                <div class="isu_bt_right">
                    <h1>A propos</h1>
                    <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, accusamus accusantium asperiores, dolor ex fugit ipsam labore magnam maiores modi molestiae nobis odio perspiciatis possimus provident quo repellendus sapiente veritatis!</h2>
                </div>
            </div>
            <div class="isu_box_bottom">
                <ul class="isu_bb_left">
                    <h4>Expériences</h4>
                   <li>
                       <h1>Senior dev ops</h1>
                       <h2>Google company</h2>
                       <h3>Jan 2023 . Juil 2019</h3>
                       <h3>3 ans & 5 mois</h3>
                   </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                </ul>
                <ul class="isu_bb_center">
                    <h4>Diplômes</h4>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                </ul>
                <ul class="isu_bb_right">
                    <h4>QQ chose jsp</h4>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                    <li>
                        <h1>Senior dev ops</h1>
                        <h2>Google company</h2>
                        <h3>Jan 2023 . Juil 2019</h3>
                        <h3>3 ans & 5 mois</h3>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</div>

<?php get_footer()?>
