<?php
?>
<footer>
    <div class="wrap2">
        <div class="footer_left">
            <h1>NEWSLETTER PERSONNALISEE</h1>
            <form action="" method="post">
                <input class="formtext_footer" type="text" placeholder="ENTER YOU'RE EMAIL">
                <input class="formsubmit_footer" type="image" src="<?= svg('/valid.svg'); ?>" alt="envoyer">
            </form>
            <div class="fl_img">
                <a href="<?= path('/'); ?>"><img src="<?= asset('/logo_icon_version.png'); ?>" alt="logo"></a>
            </div>
            <p class="fl_p1">"N'attend pas ton job de prediliction"</p>
            <p class="fl_p2">Thibault Coybes</p>
        </div>
        <div class="footer_right">
            <div class="fr_img">
                <img src="<?= svg('/footer-ordi.svg'); ?>" alt="Ordinateur">
            </div>
            <div class="fr_bottom">
                <div class="frb_left">
                    <h4>Terms And Conditions</h4>
                    <ul>
                        <li><a href="#">PRIVACY POLICY</a></li>
                        <li><a href="#">COOKIE POLICY</a></li>
                    </ul>
                </div>
                <div class="frb_right">
                    <h4>Contact</h4>
                    <ul>
                        <li><p>DREAMTEAM@GMAIL.COM</p></li>
                        <li><a href="#"><i class="fa-brands fa-instagram"></i> DREAMTREAM76</a></li>
                        <li><a href="#"><i class="fa-brands fa-square-facebook"></i> DREAMTREAM76</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script src="<?php get_template_directory_uri() . '/js/navigation.js'?>"></script>
</body>
</html>
