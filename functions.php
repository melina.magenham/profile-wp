<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
require get_template_directory() . '/inc/generale.php';
require get_template_directory() . '/inc/func.php';
require get_template_directory() . '/inc/img.php';




require get_template_directory() . '/inc/extra/template-tags.php';
require get_template_directory() . '/inc/extra/template-functions.php';



require get_template_directory() . '/inc/roles/candidat.php';
require get_template_directory() . '/inc/roles/recruteur.php';


//AJAX Profil
require get_template_directory() . '/asset/ajax/softskill.php';
require get_template_directory() . '/asset/ajax/addsoftskills.php';
require get_template_directory() . '/asset/ajax/delsoftskills.php';

require get_template_directory() . '/asset/ajax/hardskill.php';
require get_template_directory() . '/asset/ajax/addhardskills.php';
require get_template_directory() . '/asset/ajax/delhardskills.php';

require get_template_directory() . '/asset/ajax/experience.php';
require get_template_directory() . '/asset/ajax/addexperiences.php';

require get_template_directory() . '/asset/ajax/formation.php';
//require get_template_directory() . '/asset/ajax/addhardskills.php';
//require get_template_directory() . '/asset/ajax/delhardskills.php';



