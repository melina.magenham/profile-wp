<?php
global $web;
global $metas;
$metas = get_post_meta(get_the_ID());

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('chartset'); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header>
        <div class="wrap2">
            <div class="header_left">
                <div class="logo">
                    <a href="<?= path('/'); ?>"><img src="<?= asset('/logo_small_version.png'); ?>" alt="logo"></a>
                </div>
            </div>
            <div class="header_right">
                <ul>
                    <?php if (is_user_logged_in()) {
                        $current_user = wp_get_current_user();
                        $user_role = $current_user->roles[0];
                        if ($user_role == 'candidat') {
                            ?>
                            <li class="hr_createcv"><a href="<?= path('/profil'); ?>">Créer votre CV</a></li>
                            <li class="hr_display"><a href="<?= path('/profil'); ?>">Mon profil</a></li>
                            <li class="hr_display"><a href="<?php echo wp_logout_url(home_url()); ?>">Deconnexion</a></li>
                            <?php
                        } elseif ($user_role == 'recruteur') {
                            ?>
                            <li class="hr_createcv"><a href="<?= path('/recruteur'); ?>">Recruter</a></li>
                            <li class="hr_display"><a href="<?php echo wp_logout_url(home_url()); ?>">Deconnexion</a></li>
                            <?php
                        } elseif ($user_role == 'administrator') {
                            ?>
                            <li class="hr_createcv"><a href="<?= path('/recruteur'); ?>">Recruter</a></li>
                            <li class="hr_display"><a href="<?= path('/profil'); ?>">Mon profil</a></li>
                            <li class="hr_display"><a href="<?php echo wp_logout_url(home_url()); ?>">Deconnexion</a></li>
                            <?php
                        }
                    } else {
                        ?>
                        <li class="hr_createcv"><a href="" id="openModal3">Créer votre CV</a></li>
                        <li class="hr_display"><a href="" id="openModal">Connexion</a></li>
                        <li class="hr_display"><a href="<?= path('/inscription'); ?>">Inscription</a></li>
                    <?php } ?>
                    <li class="hr_display_none"><a href="#" id="opener_burger"><i class="fa-solid fa-user"></i></a></li>

                </ul>
            </div>


        </div>

        <div id="menu_burger">
            <ul>
                <?php if (is_user_logged_in()) { ?>
                    <li><a href="#">Mon profil</a></li>
                    <li><a href="<?php echo wp_logout_url(home_url()); ?>">Deconnexion</a></li>
                <?php } else { ?>
                    <li class="hr_createcv"><a href="#">Créer votre CV</a></li>
                    <li class="hr_display"><a href="#">Mon profil</a></li>
                    <li class="hr_display"><a href="<?php echo wp_logout_url(home_url()); ?>">Déconnexion</a></li>
                <?php } ?>
            </ul>
            <a id="closer" href="">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M175 175C184.4 165.7 199.6 165.7 208.1 175L255.1 222.1L303 175C312.4 165.7 327.6 165.7 336.1 175C346.3 184.4 346.3 199.6 336.1 208.1L289.9 255.1L336.1 303C346.3 312.4 346.3 327.6 336.1 336.1C327.6 346.3 312.4 346.3 303 336.1L255.1 289.9L208.1 336.1C199.6 346.3 184.4 346.3 175 336.1C165.7 327.6 165.7 312.4 175 303L222.1 255.1L175 208.1C165.7 199.6 165.7 184.4 175 175V175zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z" />
                </svg>
            </a>
        </div>

        <div id="modal" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                <h1>De retour!</h1>
                <h3>Connecte toi avec tes identifiants</h3>
                <form action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>" method="post">
                    <div class="form_email">
                        <input type="text" name="log" placeholder="E-mail ou identifiant">
                        <div class="form_round"></div>
                    </div>
                    <div class="form_password">
                        <input type="password" id="password-field" name="pwd" placeholder="Mot de passe">
                        <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                        <div class="form_round"></div>
                    </div>
                    <div class="form_valid">
                        <input type="submit" name="wp-submit" value="CONNEXION">
                        <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url()); ?>">
                    </div>
                </form>
                <ul class="modal_socialmedias">
                    <li><a href="#"><i class="fa-brands fa-whatsapp"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-snapchat"></i></a></li>
                </ul>
            </div>
        </div>

        <div id="modal2" class="modal">
            <div class="modal-content">
                <span class="close2">&times;</span>
                <h1>Oups!</h1>
                <h3>Connectes toi pour créer ton premier CV!</h3>
                <form action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>" method="post">
                    <div class="form_email">
                        <input type="text" name="log" placeholder="E-mail ou identifiant">
                        <div class="form_round"></div>
                    </div>
                    <div class="form_password">
                        <input type="password" id="password-field" name="pwd" placeholder="Mot de passe">
                        <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                        <div class="form_round"></div>
                    </div>
                    <div class="form_valid">
                        <input type="submit" name="wp-submit" value="CONNEXION">
                        <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url()); ?>">
                    </div>
                </form>
                <ul class="modal_socialmedias">
                    <li><a href="#"><i class="fa-brands fa-whatsapp"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-snapchat"></i></a></li>
                </ul>
            </div>
        </div>

        <div id="menu_burger">
            <ul>
                <?php if (is_user_logged_in()) { ?>
                    <li><a href="#">Mon profil</a></li>
                    <li><a href="<?php echo wp_logout_url(home_url()); ?>">Déconnexion</a></li>
                <?php } else { ?>
                    <li class="hr_createcv"><a id="openModal3" href="#">Créer votre CV</a></li>
                    <li class="hr_display"><a href="" id="openModal">Connexion</a></li>
                    <li class="hr_display"><a href="#">Inscription</a></li>
                    <li><a id="openModal2" href="#">Connexion</a></li>
                    <li><a href="#">Inscription</a></li>
                <?php } ?>
            </ul>
            <a id="closer" href="">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M175 175C184.4 165.7 199.6 165.7 208.1 175L255.1 222.1L303 175C312.4 165.7 327.6 165.7 336.1 175C346.3 184.4 346.3 199.6 336.1 208.1L289.9 255.1L336.1 303C346.3 312.4 346.3 327.6 336.1 336.1C327.6 346.3 312.4 346.3 303 336.1L255.1 289.9L208.1 336.1C199.6 346.3 184.4 346.3 175 336.1C165.7 327.6 165.7 312.4 175 303L222.1 255.1L175 208.1C165.7 199.6 165.7 184.4 175 175V175zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z" />
                </svg>
            </a>
        </div>
    </header>
    </div>

    <div id="modal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <h1>De retour!</h1>
            <h3>Connecte toi avec tes identifiants</h3>
            <form action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>" method="post">
                <div class="form_email">
                    <input type="text" name="log" placeholder="E-mail ou identifiant">
                    <div class="form_round"></div>
                </div>
                <div class="form_password">
                    <input type="password" id="password-field" name="pwd" placeholder="Mot de passe">
                    <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                    <div class="form_round"></div>
                </div>
                <div class="form_valid">
                    <input type="submit" name="wp-submit" value="CONNEXION">
                    <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url()); ?>">
                </div>
            </form>
            <ul class="modal_socialmedias">
                <li><a href="#"><i class="fa-brands fa-whatsapp"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-snapchat"></i></a></li>
            </ul>
        </div>
    </div>

    <div id="modal2" class="modal">
        <div class="modal-content">
            <span class="close2">&times;</span>
            <h1>Oups!</h1>
            <h3>Connectes toi pour créer ton premier CV!</h3>
            <form action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>" method="post">
                <div class="form_email">
                    <input type="text" name="log" placeholder="E-mail ou identifiant">
                    <div class="form_round"></div>
                </div>
                <div class="form_password">
                    <input type="password" id="password-field" name="pwd" placeholder="Mot de passe">
                    <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                    <div class="form_round"></div>
                </div>
                <div class="form_valid">
                    <input type="submit" name="wp-submit" value="CONNEXION">
                    <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url()); ?>">
                </div>
            </form>
            <ul class="modal_socialmedias">
                <li><a href="#"><i class="fa-brands fa-whatsapp"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa-brands fa-snapchat"></i></a></li>
            </ul>
        </div>
    </div>

    <div id="menu_burger">
        <ul>
            <?php if (is_user_logged_in()) { ?>
                <li><a href="#">Mon profil</a></li>
                <li><a href="<?php echo wp_logout_url(home_url()); ?>">Deconnexion</a></li>
            <?php } else { ?>
                <li><a id="openModal2" href="#">Connexion</a></li>
                <li><a href="#">Inscription</a></li>
            <?php } ?>
        </ul>
        <a id="closer" href="">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path d="M175 175C184.4 165.7 199.6 165.7 208.1 175L255.1 222.1L303 175C312.4 165.7 327.6 165.7 336.1 175C346.3 184.4 346.3 199.6 336.1 208.1L289.9 255.1L336.1 303C346.3 312.4 346.3 327.6 336.1 336.1C327.6 346.3 312.4 346.3 303 336.1L255.1 289.9L208.1 336.1C199.6 346.3 184.4 346.3 175 336.1C165.7 327.6 165.7 312.4 175 303L222.1 255.1L175 208.1C165.7 199.6 165.7 184.4 175 175V175zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z" />
            </svg>
        </a>
    </div>
    </header>