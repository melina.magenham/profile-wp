<?php

/**
 * Register a custom post type called "header".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_header_init() {
    $labels = array(
        'name'                  => _x( 'headers', 'Post type general name', 'profile' ),
        'singular_name'         => _x( 'header', 'Post type singular name', 'profile' ),
        'menu_name'             => _x( 'header', 'Admin Menu text', 'profile' ),
        'name_admin_bar'        => _x( 'header', 'Add New on Toolbar', 'profile' ),
        'add_new'               => __( 'Ajouter un image', 'profile' ),
        'add_new_item'          => __( 'Add New header', 'profile' ),
        'new_item'              => __( 'New header', 'profile' ),
        'edit_item'             => __( 'Edit header', 'profile' ),
        'view_item'             => __( 'View header', 'profile' ),
        'all_items'             => __( 'All headers', 'profile' ),
        'search_items'          => __( 'Search headers', 'profile' ),
        'parent_item_colon'     => __( 'Parent headers:', 'profile' ),
        'not_found'             => __( 'No headers found.', 'profile' ),
        'not_found_in_trash'    => __( 'No headers found in Trash.', 'profile' ),
        'featured_image'        => _x( 'header Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'profile' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'profile' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'profile' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'profile' ),
        'archives'              => _x( 'header archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'profile' ),
        'insert_into_item'      => _x( 'Insert into header', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'profile' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this header', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'profile' ),
        'filter_items_list'     => _x( 'Filter headers list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'profile' ),
        'items_list_navigation' => _x( 'headers list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'profile' ),
        'items_list'            => _x( 'headers list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'profile' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'header' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 45,
        'menu_icon'          => 'dashicons-slides',
        'supports'           => array( 'title','excerpt','thumbnail'),
    );

    register_post_type( 'header', $args );
}

add_action( 'init', 'wpdocs_codex_header_init' );