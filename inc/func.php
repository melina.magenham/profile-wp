<?php

function asset($file)
{
    return get_template_directory_uri() . '/asset/img/' . $file;
}

function svg($file)
{
    return get_template_directory_uri() . '/asset/svg/' . $file;
}

function path($slug)
{
    return esc_url(home_url($slug));
}

function validationEmail($errors, $email, $entry = 'email')
{
    if (!empty($email)) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[$entry] = 'L\'email n\'est pas valide';
        }
    } else {
        $errors[$entry] = 'Ce champ est obligatoire';
    }
    return true;
}
function validationText($err, $data, $keyError, $min, $max)
{
    if (!empty($data)) {
        if (mb_strlen($data) < $min) {
            $err[$keyError] = 'Veuillez renseigner plus de ' . $min . ' caractères';
        } elseif (mb_strlen($data) > $max) {
            $err[$keyError] = 'Veuillez renseigner moins de ' . $max . ' caractères';
        } else {
            return true;
        }
    } else {
        $err[$keyError] = 'Veuillez renseigner ce champ';
    }
    return $err;
}

function web_r($meta, $key)
{
    if (!empty($meta[$key][0])) {
        return $meta[$key][0];
    }
    return '';
}

function getImageById($id_img, $size, $alt = '', $class = '')
{
    $img = wp_get_attachment_image_src($id_img, $size);
    if (!empty($img)) {
        if (!empty($class)) {
            return '<img src="' . $img[0] . '" alt="' . $alt . '" class="' . $class . '" />';
        } else {
            return '<img src="' . $img[0] . '" alt="' . $alt . '" />';
        }
    }
}

function debug($var, $height = 200, $fixed = false)
{
    $backt = debug_backtrace()[0];
    if ($fixed) {
        echo '<pre style="position: fixed;top:0;left:0;right:0;height:' . $height . 'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 220px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    } else {
        echo '<pre style="height:' . $height . 'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 10px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    }
    echo '<p style="font-size:.85rem;">' . $backt['file'] . ' - ' . $backt['line'] . '</p>';
    print_r($var);
    echo '</pre>';
}
