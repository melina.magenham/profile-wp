<?php

/**
 * examen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package examen
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.2');
}

function examen_setup()
{

    load_theme_textdomain('examen', get_template_directory() . '/languages');
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    register_nav_menus(
        array(
            'menu-1' => esc_html__('Primary', 'examen'),
        )
    );
}
add_action('after_setup_theme', 'examen_setup');

function examen_content_width()
{
    $GLOBALS['content_width'] = apply_filters('examen_content_width', 640);
}
add_action('after_setup_theme', 'examen_content_width', 0);


function examen_widgets_init()
{
    register_sidebar(
        array(
            'name'          => esc_html__('Sidebar', 'examen'),
            'id'            => 'sidebar-1',
            'description'   => esc_html__('Add widgets here.', 'examen'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action('widgets_init', 'examen_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function examen_scripts()
{
    wp_enqueue_style('examen-style', get_stylesheet_uri(), array(), _S_VERSION);


    if (is_page_template('template-inscription.php')) {
        wp_enqueue_style('style-css', get_template_directory_uri() . './asset/css/inscription.css', array(), _S_VERSION);
    }



    // JS
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js', array(), _S_VERSION, true);
    if (is_user_logged_in()) {
        wp_deregister_script('js-header');
    } else {
        wp_enqueue_script('js-header', get_template_directory_uri() . '/asset/js/header.js', array('jquery'), _S_VERSION, true);
    }
    wp_enqueue_script('js-burger', get_template_directory_uri() . '/asset/js/burger.js', array('jquery'), _S_VERSION, true);
}
add_action('wp_enqueue_scripts', 'examen_scripts');

function styles_scripts_profil()
{

    wp_enqueue_style('exam-style', get_stylesheet_uri(), array(), _S_VERSION);


    if (is_page_template('template-profil.php')) {
        //css
        wp_enqueue_style('style-css', get_template_directory_uri() . './asset/css/profil.css', array(), _S_VERSION);
        //js
        wp_enqueue_script('gestiondesonglet.js', get_template_directory_uri() . '/asset/js/profil/gestiondesonglet.js', array(), _S_VERSION, true);
        wp_enqueue_script('resume.js', get_template_directory_uri() . '/asset/js/profil/resume.js', array(), _S_VERSION, true);
        wp_enqueue_script('info_information.js', get_template_directory_uri() . '/asset/js/profil/info_information.js', array(), _S_VERSION, true);
        wp_enqueue_script('info_softskills.js', get_template_directory_uri() . '/asset/js/profil/info_softskills.js', array(), _S_VERSION, true);
        wp_enqueue_script('info_hardskills.js', get_template_directory_uri() . '/asset/js/profil/info_hardskills.js', array(), _S_VERSION, true);
        wp_enqueue_script('info_experiencepro.js', get_template_directory_uri() . '/asset/js/profil/info_experiencepro.js', array(), _S_VERSION, true);
        wp_enqueue_script('info_diplome.js', get_template_directory_uri() . '/asset/js/profil/info_diplome.js', array(), _S_VERSION, true);
    }
    if (is_page_template('template-recrutement.php')) {
        //css
        wp_enqueue_style('style-css', get_template_directory_uri() . './asset/css/profil.css', array(), _S_VERSION);
        //js
    }
    if (is_page_template('single-user.php')) {
        //css
        wp_enqueue_style('style-css', get_template_directory_uri() . './asset/css/profil.css', array(), _S_VERSION);
        wp_enqueue_style('user-css', get_template_directory_uri() . './asset/css/single-user.css', array(), _S_VERSION);
        //js
        wp_enqueue_script('user-js', get_template_directory_uri() . './asset/js/single-user.js', array(), _S_VERSION, true);
    }
}
add_action('wp_enqueue_scripts', 'styles_scripts_profil');


add_filter('rest_authentication_errors', function ($result) {
    if (!empty($result)) {
        return $result;
    }
    if (!is_user_logged_in()) {
        return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', array('status' => 401));
    }
    return $result;
});
