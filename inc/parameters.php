<?php
global $web;
$web = array(
    'page' => [
        'homes' => [
            'id' => 6,
            'slug' => '/',
        ],
        'blog' => [
            'id' => 8,
            'slug' => 'blog',
        ],
        'connexion' => [
            'id' => 9,
            'slug' => 'connexion',
        ],
        'inscription' => [
            'id' => 10,
            'slug' => 'inscription',
        ],
        'mentions' => [
            'id' => 7,
            'slug' => 'mentions-legales',
        ],
        'cv' => [
            'id' => 11,
            'slug' => 'new-cv',
        ],
        'profil' => [
            'id' => 12,
            'slug' => 'profil',
        ],
        'information' => [
            'id' => 20,
            'slug' => 'information',
        ],
        'mescv' => [
            'id' => 21,
            'slug' => 'mescv',
        ],
        'reglage' => [
            'id' => 24,
            'slug' => 'reglage',
        ],
        'Rectrutement' => [
            'id' => 13,
            'slug' => 'rectrutement',
        ],

    ],
);