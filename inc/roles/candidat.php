
<?php
add_role('candidat', __(
    'Candidat'),
    array(
        'read'            => true, // Allows a user to read
        'edit_posts'      => true, // Allows user to edit their own posts
        'edit_others_posts' => true, // Allows user to edit others posts too
        'create_posts'    => true, // Allows user to create new posts
        'manage_categories' => true, // Allows user to manage post categories
        'publish_posts'   => true, // Allows the user to publish posts
        'edit_published_posts' => true, // Allows the user to edit published posts
    )
);

