<div id="modal" class="modal">
    <form action="<?php echo esc_url(site_url('wp-login.php', 'login_post')); ?>" method="post">
        <input type="text" name="log" placeholder="Username">
        <input type="password" name="pwd" placeholder="Password">
        <input type="submit" name="wp-submit" value="Submit">
        <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url()); ?>">
    </form>
</div>