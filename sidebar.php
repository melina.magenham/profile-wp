<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package examen
 */
/* Template Name: Sidebar */

global $web;
global $metas;
?>

<aside id="secondary" class="widget-area">
    <div class="onglets">
        <section id="sidebar">
            <?php if (!empty($metas['img_profil'][0])) {
                echo getImageById($metas['img_profil'][0], 'img_profil' , 'alt', 'img_profil');
            } ?>
            <div class="idendity">
                <h2>name</h2>
                <h3>status</h3>
                <h4>lorem</h4>
            </div>
            <div class="container_all">
                <nav class="container-onglets">
                    <ul>
                        <li class="onglets active" id="onglet1" data-anim="1">Resume</li>
                        <li class="onglets" id="onglet2" data-anim="2">Mes Informations</li>
                        <li class="onglets" id="onglet3" data-anim="3">Mes CV</li>
                        <li class="onglets" id="onglet4" data-anim="4">Réglages</li>
                    </ul>
                </nav>
            </div>
            <div class="help">
                <div class="style_help">
                    <div class="txt_help">
                        <h2>Besoin d’aide ?</h2>
                        <h3>Visitez notre FAQ !</h3>
                    </div>
                    <div class="txt_button">
                        <p type="button">Clique ici</p>
                    </div>
                    <?php if (!empty($metas['img_help'][0])) {
                        echo getImageById($metas['img_help'][0], 'img_help' , 'alt', 'img_help');
                    } ?>
                </div>
            </div>
        </section>
    </div>

</aside><!-- #secondary -->
