<?php

/* Template Name: Single user */
get_header();
global $web;
$user_id = intval($_GET['id']);
if (!$user_id) {
    echo 'L\'ID de l\'utilisateur est manquant';
    get_footer();
    exit;
}
$user = get_user_by('ID', $user_id);
if (!$user) {
    echo 'L\'utilisateur n\'existe pas';
    get_footer();
    exit;
}
?>
<section id="resume">
    <div class="wrap">
        <div class="container">
            <div class="profil">
                <div class="presentation">
                    <div class="present">
                        <div class="img-user">
                            <img src="<?= get_avatar_url($user->ID) ?>" class="pic_profil" alt="">
                        </div>
                        <div class="info-user stat">
                            <h2 class="name"><?php echo $user->first_name . " " . $user->last_name; ?></h2>
                            <h3 class="status"><?php echo $user->role; ?></h3>
                        </div>
                        <div class="stat">
                            <p class="block"></p>
                            <h2>Ce cv à été consulté<span>251 vues</span></h2>
                        </div>
                    </div>
                    <div class="intro">
                        <h2 class="title">A propos</h2>
                        <p class="txt">Je suis un développeur passionné par l'automatisation et la mise en production efficace des applications. En tant que DevOps, je suis à la fois un développeur et un administrateur système, ce qui me permet de comprendre les défis techniques auxquels les équipes sont confrontées.
                            Je suis spécialisé dans la mise en place d'une infrastructure résiliente et scalable, ainsi que dans l'intégration continue et la livraison continue. Je suis également expérimenté dans la mise en place de processus de tests automatisés pour garantir la qualité du code.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="boxs">
            <div class="competence">
                <h2 class="title">HardSkill</h2>
                <div id="hardskill"></div>
                <line></line>
            </div>
            <div class="competence">
                <h2 class="title">Softskill</h2>
                <div id="competence"></div>
                <line></line>
            </div>
        </div>
        <div class="boxs boxlarge">
            <div class="competence">
                <h2 class="title">Expérience Professionel</h2>
                <div id="experience"></div>
                <line></line>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
