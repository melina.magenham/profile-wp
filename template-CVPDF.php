<?php
/* Template Name: CVPDF */

require 'vendor/autoload.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();

ob_start();
require_once "template-PDF.php";
$html = ob_get_contents();
ob_end_clean();
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4');

// Render le HTML en PDF
$dompdf->render();

// Sortie en PDF depuis le navigateur
$dompdf->stream();