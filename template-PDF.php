<?php
/* Template Name: PDF */



?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>CV</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 50px;
        }
        th, td {
            padding: 10px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
        h1 {
            text-align: center;
            margin-bottom: 50px;
        }
        .sidebar {
            float: left;
            width: 20%;
            background-color: #f1f1f1;
            padding: 20px;
            height: 100%;
        }
        .main-content {
            float: left;
            width: 80%;
            padding: 20px;
        }
        .clear {
            clear: both;
        }
        .skill {
            width: 49%;
            float: left;
        }
        .skill h2 {
            margin-bottom: 10px;
        }
        .skill ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div class="sidebar">
    <table>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>Votre date de naissance</td>
        </tr>
        <tr>
            <td>Votre ville</td>
        </tr>
        <tr>
            <td>Votre code postal</td>
        </tr>
    </table>
</div>
<div class="main-content">
    <h2>Introduction</h2>
    <p>Une petite description de vous-même</p>
    <br><br>
    <div class="skill">
        <h2>Les Softskills</h2>
        <ul>
            <li>Compétence 1</li>
            <li>Compétence 2</li>
            <li>Compétence 3</li>
        </ul>
    </div>
    <div class="skill">
        <h2>Les Hardskills</h2>
        <ul>
            <li>Compétence 1</li>
            <li>Compétence 2</li>
            <li>Compétence 3</li>
        </ul>
    </div>
    <div class="clear"></div>
    <br><br>
    <div class="clear"></div>
    <br><br>