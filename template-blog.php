<?php
/* Template Name: Blog */
?>
<section id="blog">
    <div class="wrap2">
        <div class="blog_left">
            <h1 class="blt_h1">Bienvenue ches ProFile<b> : Actualités</b></h1>
            <h2 class="blt_h2">Les plus récents</h2>
            <div class="blog_recents">
                <div class="blog_themostrecent">
                    <?php


                    $args = array(
                        'post_status' => 'article',
                        'showposts' => 1,
                        'orderby' => 'post_date',
                    );

                    $the_query = new WP_Query($args);
                    // The Loop
                    if ($the_query->have_posts()) {
                        while ($the_query->have_posts()) {
                            $the_query->the_post();

                            echo '
                                <div class="tmr_one">
                                    <img src="' . get_the_post_thumbnail_url('0', 'image_big') . '">
                                    <a href="' . get_the_permalink() . '">
                                        <p>' . get_the_title() . '</p>
                                    </a>
                                </div>
                            ';
                        }
                    } else {
                        // no posts found
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();
                    ?>

                </div>
                <div class="blog_mostrecent">
                    <?php


                    $args2 = array(
                        'post_status' => 'article',
                        'posts_per_page' => '4',
                        'orderby' => 'post_date',
                    );

                    $the_query = new WP_Query($args2);
                    // The Loop
                    if ($the_query->have_posts()) {
                        while ($the_query->have_posts()) {
                            $the_query->the_post();
                            echo '
                            <a href="' . get_the_permalink() . '" class="tm_one">
                                <img src="' . get_the_post_thumbnail_url('0', 'image_small') . '">
                                <h2>' . get_the_title() . '</h2>
                                <div class="tmo_bottom">
                                    <p>' . get_the_time('d/m/Y') . '</p>
                                    <p>' . get_the_excerpt() . '</p>
                                </div>
                            </a>
                            ';
                        }
                    } else {
                        // no posts found
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
        <div class="blog_right">
            <div class="br_title">
                <h1>Les plus consultés</h1>
            </div>
            <div class="blog_mostvisited_all">

                <?php

                $args3 = array(
                    'post_status' => 'article',
                    'posts_per_page' => '5',
                    'orderby' => rand(),
                );

                $the_query = new WP_Query($args3);
                $count = 0;
                // The Loop
                if ($the_query->have_posts()) {
                    while ($the_query->have_posts()) {
                        $count++;
                        $the_query->the_post();
                        echo '
                    <a href="' . get_the_permalink() . '" class="blog_mv_one">
                        <div class="bmvo_left">
                            <p>' . $count . '</p>
                        </div>
                        <div class="bvmo_right">
                            <h1>' . get_the_title() . '</h1>
                            <p>' . get_the_time('d/m/Y') . '</p>
                        </div>
                    </a>
                    ';
                    }
                } else {
                    // no posts found
                }
                /* Restore original Post Data */
                wp_reset_postdata();
                ?>
            </div>
            <div class="br_bottom">
                <h1>Catégories</h1>
                <p>Trouvez ce dont vous avez besoin pour reussir dans votre carrière!</p>
                <ul>
                    <li><a href="#">Informations</a></li>
                    <li><a href="#">Recrutement</a></li>
                    <li><a href="#">Emplois</a></li>
                    <li><a href="#">Faire son CV</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>