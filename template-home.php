<?php
/* Template Name: Homepage */
get_header();
include 'modal.php' ?>

<!--  INTRO  -->
<section id="intro">
    <div class="wrap2">
        <div class="intro">
            <img src="<?= asset('/background-s1.jpg'); ?>" alt="grosse image de fond">
            <h1>Il n'a jamais été aussi facile d'organiser ton CV!</h1>
        </div>
    </div>
</section>

<!--  RECRUTEUR  -->
<section id="recruteur">
    <div class="wrap2">
        <div class="all_rectuteur_left">
            <div class="all_recruteur_title">
                <h1>Des Entreprises</h1>
                <h2>Qui Recrutent.</h2>
            </div>
            <div class="one_recruteur">
                <div class="retruteur_img">
                    <img src="<?= asset('/entreprise1.jpg'); ?>" alt="rectureur">
                </div>
                <a href="#" class="one_rectuteur_content">
                    <div class="one_recruteur_content1">
                        <h2>Thelem Assurance</h2>
                        <p>894 job</p>
                    </div>
                    <div class="recruteur_button">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="one_recruteur">
                <div class="retruteur_img">
                    <img src="<?= asset('/entreprise2.jpg'); ?>" alt="rectureur">
                </div>
                <a href="#" class="one_rectuteur_content">
                    <div class="one_recruteur_content1">
                        <h2>Thelem Assurance</h2>
                        <p>894 job</p>
                    </div>
                    <div class="recruteur_button">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </a>
            </div>
        </div>

        <div class="all_rectuteur_center">
            <div class="one_recruteur">
                <div class="retruteur_img">
                    <img src="<?= asset('/entreprise3.jpg'); ?>" alt="rectureur">
                </div>
                <a href="#" class="one_rectuteur_content">
                    <div class="one_recruteur_content1">
                        <h2>Thelem Assurance</h2>
                        <p>894 job</p>
                    </div>
                    <div class="recruteur_button">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="one_recruteur">
                <div class="retruteur_img">
                    <img src="<?= asset('/entreprise2.jpg'); ?>" alt="rectureur">
                </div>
                <a href="#" class="one_rectuteur_content">
                    <div class="one_recruteur_content1">
                        <h2>Thelem Assurance</h2>
                        <p>894 job</p>
                    </div>
                    <div class="recruteur_button">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="arc_link">
                <a href="#">Voir Plus</a>
            </div>
            <div class="arc_svg">
                <img src="<?= svg('/arrow.svg'); ?>" alt="arrow">
            </div>
        </div>

        <div class="all_rectuteur_right">
            <div class="all_recruteur_title">
                <h1>Trouve</h1>
                <h2>La Tienne.</h2>
            </div>
            <div class="one_recruteur">
                <div class="retruteur_img">
                    <img src="<?= asset('/entreprise3.jpg'); ?>" alt="rectureur">
                </div>
                <a href="#" class="one_rectuteur_content">
                    <div class="one_recruteur_content1">
                        <h2>Thelem Assurance</h2>
                        <p>894 job</p>
                    </div>
                    <div class="recruteur_button">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </a>
            </div>
            <div class="one_recruteur">
                <div class="retruteur_img">
                    <img src="<?= asset('/entreprise1.jpg'); ?>" alt="rectureur">
                </div>
                <a href="#" class="one_rectuteur_content">
                    <div class="one_recruteur_content1">
                        <h2>Thelem Assurance</h2>
                        <p>894 job</p>
                    </div>
                    <div class="recruteur_button">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!--  BANNER  -->
<section id="banner">
    <div class="wrap3">
        <div class="banner_left">
            <div class="banner_img">
                <img src="<?= asset('/logo_small_version.png'); ?>" alt="logo">
            </div>
        </div>
        <div class="banner_right">
            <div class="banner_r_display_none">
                <h1>Préparez vous à</h1>
                <h2>Décrocher votre job!</h2>
            </div>
            <ul>
                <li>
                    <a href="#">
                        <h4>Parcourez les offres d'emploi</h4>
                        <i class="fa-solid fa-caret-right"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <h4>Trouvez votre entreprise</h4>
                        <i class="fa-solid fa-caret-right"></i>
                    </a>
                </li>
                <li class="br_link_display_block">
                    <a href="#">
                        <h4>Explorez notre article</h4>
                        <i class="fa-solid fa-caret-right"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="banner_background">
            <img src="<?= asset('/img_s3.png'); ?>" alt="logo">
        </div>
    </div>
</section>

<!--  EXPLAIN  -->
<section id="explain">
    <div class="wrap2">
        <div class="explain_title">
            <h1>Préparez-vous à</h1>
            <h2>décrocher votre job!</h2>
        </div>
        <div class="explain_all">
            <!--ZONE HAUT-->
            <div class="explain_top">
                <!--BOX LEFT-->
                <div class="et_left">
                    <div class="etl_top">
                        <h1>150 689</h1>
                        <div class="etl_img">
                            <img src="<?= svg('/ordinateur.svg'); ?>" alt="ordinateur">
                        </div>
                    </div>
                    <div class="etl_bottom">
                        <h2>CV lus en moyenne chaque semaine,</h2>
                        <h3>soyez le prochain à être vu!</h3>
                    </div>
                </div>
                <!--BOX RIGHT-->
                <div class="et_right">
                    <a href="#">Créer mon CV</a>
                </div>
            </div>
            <!--ZONE BAS-->
            <div class="explain_bottom">
                <!--BOX LEFT-->
                <div class="eb_left">
                    <div class="ebl_top">
                        <h1>Emplois posté toutes les semaines,</h1>
                        <h2>à vous de trouver le votre!</h2>
                    </div>
                    <div class="ebl_bottom">
                        <h1>67 000</h1>
                    </div>
                </div>
                <!--BOX RIGHT-->
                <div class="eb_right">
                    <a href="">Créer mon alerte</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--  BLOG  -->
<?php
include 'template-blog.php';
get_footer()
?>