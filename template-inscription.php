<?php
/* Template Name: Inscription */
get_header();


function viewError($errors, $key)
{
    if (!empty($errors[$key])) {
        echo $errors[$key];
    }
}
$errors = [];




if (!empty($_POST['submit'])) {
    $errors = [];
    if (!empty($_POST['checkbox'])) {
        if (!empty($_POST['g-recaptcha-response'])) {
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $adresse = $_POST['adresse'];
            $ville = $_POST['ville'];
            $codepostal = $_POST['codepostal'];
            $naissance_date = $_POST['naissance-date'];
            $genre = $_POST['genre'];
            $identifiant = $_POST['identifiant'];
            $email = $_POST['email'];
            $mdp = $_POST['mdp'];
            $mdp_confirmation = $_POST['mdp-confirmation'];

            // Validation des données
            if (!empty($nom && $prenom && $adresse && $ville && $genre && $codepostal && $naissance_date && $identifiant && $email && $mdp && $mdp_confirmation)) {
                // Vérification de la disponibilité de l'identifiant
                if (username_exists($identifiant)) {
                    echo "L'identifiant existe déjà, veuillez en choisir un autre";
                } else {
                    // Vérification de l'identifiant
                    if (validationText($errors, $identifiant, 'identifiant', 2, 30) !== true) {
                        if (mb_strlen($identifiant) >= 15) {
                            $errors['identifiant'] = 'Votre identifiant est trop long';
                        } elseif (mb_strlen($identifiant) <= 2) {
                            $errors['identifiant'] = 'Votre identifiant est trop court';
                        }
                    }
                    // Vérification de la correspondance des mots de passe
                    if ($mdp !== $mdp_confirmation) {
                        $errors['mdp-confirmation'] =  "Les mots de passe ne correspondent pas";
                    } else if (validationText($errors, $mdp_confirmation, 'mdp-confirmation', 5, 50)) {
                        if (mb_strlen($mdp) <= 5) {
                            $errors['mdp-confirmation'] = 'Votre mot de passe est trop court';
                        } elseif (mb_strlen($mdp) >= 50) {
                            $errors['mdp-confirmation'] = 'Votre mot de passe est trop long';
                        }
                    }
                    // Vérification Email
                    if (validationEmail($errors, $email) !== true) {
                        $errors['email'] = 'Votre email est invalide';
                    }

                    // Validation code postal

                    if (mb_strlen($codepostal) > 5) {
                        $errors['codepostal'] = 'Veuillez renseigner un CodePostal valide';
                    }

                    // Validation nom

                    if (validationText($errors, $nom, 'nom', 2, 20)) {
                        if (mb_strlen($nom) < 2) {
                            $errors['nom'] = 'Votre nom est trop court';
                        } else if (mb_strlen($nom) > 20) {
                            $errors['nom'] = 'Votre nom est trop long';
                        }
                    }

                    // Validation prenom

                    if (validationText($errors, $prenom, 'prenom', 2, 20)) {
                        if (mb_strlen($prenom) < 2) {
                            $errors['prenom'] = 'Votre prenom est trop court';
                        } else if (mb_strlen($prenom) > 20) {
                            $errors['prenom'] = 'Votre prenom est trop long';
                        }
                    }

                    if (empty($errors)) {
                        // Création de l'utilisateur
                        $userdata = array(
                            'user_login' => $identifiant,
                            'user_email' => $email,
                            'user_pass' => $mdp,
                            'first_name' => $prenom,
                            'last_name' => $nom,
                            'nickname' => $prenom . " " . $nom
                        );
                        // Ajout de meta custom
                        $user = wp_insert_user($userdata);
                        if (!is_wp_error($user)) {
                            $meta = array(
                                'adresse' => $adresse,
                                'ville' => $ville,
                                'codepostal' => $codepostal,
                                'naissance-date' => $naissance_date,
                                'genre' => $genre,
                                'identifiant' => $identifiant,
                            );
                            foreach ($meta as $key => $val) {
                                update_user_meta($user, $key, $val);
                            }
                            echo '<p>' . "L'utilisateur a été créé avec succès" . '</p>';
                        } else {
                            echo '<p>' . "Erreur lors de la création " . '</p>';
                        }
                    }
                }
            } else {

                $errors['err-all'] = 'Veuillez remplir tout les champs';

                // Création de l'utilisateur
                $userdata = array(
                    'user_login' => $identifiant,
                    'user_email' => $email,
                    'user_pass' => $mdp,
                    'first_name' => $prenom,
                    'last_name' => $nom,
                    'nickname' => $prenom . " " . $nom,
                );

                $user = wp_insert_user($userdata);
                if (!is_wp_error($user)) {
                    $meta = array(
                        'adresse' => $adresse,
                        'ville' => $ville,
                        'codepostal' => $codepostal,
                        'naissance-date' => $naissance_date,
                        'genre' => $genre,
                        'identifiant' => $identifiant
                    );
                    foreach ($meta as $key => $val) {
                        update_user_meta($user, $key, $val);
                    }
                    echo '<p>' . "L'utilisateur a été créé avec succès" . '</p>';
                } else {
                    echo '<p>' . "Erreur lors de la création " . '</p>';
                }
            }
        } else {
            $errors['captcha'] = 'Veuillez faire la vérification Captcha';
        }
    } else {
        $errors['checkbox'] = 'Veuillez déclarer avoir pris connaissance des conditions générales d\'utilisation';
    }
}



?>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<section id="baniere-inscription">
    <div class="wrap">
        <div class="container">
            <div class="left">
                <h1>Inscription</h1>
            </div>
            <div class="img">
                <img src="<?= asset('signIn-Logo.svg') ?>" alt="">
            </div>
            <div class="right">
                <img src="<?= asset('logo_small_version.png') ?>" alt="">
            </div>
        </div>
    </div>
</section>
<form action="" method="post">
    <section id="incription--firstForm">
        <div class="wrap">
            <div class="container">
                <h2>Qui êtes-vous ?</h2>
                <div class="first-container">
                    <div class="left">
                        <label for="nom">Nom :</label>
                        <input type="text" id="nom" class="nom" name="nom">
                    </div>

                    <div class="right">
                        <label for="prenom">Prenom :</label>
                        <input type="text" id="prenom" class="prenom" name="prenom">
                    </div>
                </div>

                <div class="mid-container">
                    <label for="adresse">Adresse :</label>
                    <input type="text" id="adresse" class="adresse" name="adresse">
                </div>

                <div class="second-container">
                    <div class="top">
                        <div class="left">
                            <label for="ville">Ville :</label>
                            <input type="text" id="ville" class="ville" name="ville">
                        </div>
                        <div class="right">
                            <label for="codepostal">Code postal :</label>
                            <input type="text" id="codepostal" class="codepostal" name="codepostal">
                            <span id="error" class="errors"><?php viewError($errors, 'codepostal');  ?></span>
                        </div>
                    </div>
                    <div class="bot">
                        <div class="left">
                            <label for="naissance-date">Date de naissance :</label>
                            <input type="date" id="naissance-date" class="naissance-date" name="naissance-date">
                        </div>

                        <div class="right">
                            <label for="genre">Genre :</label>
                            <select name="genre" id="genre">
                                <option value="">--Genre--</option>
                                <option value="Homme">Homme</option>
                                <option value="Femme">Femme</option>
                                <option value="Autre">Autre</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <section id="inscription--secondForm">
        <div class="wrap">
            <div class="container">
                <h2>Identifiant</h2>
                <div class="first-container">
                    <div class="top">
                        <label for="identifiant">Identifiant :</label>
                        <input type="text" id="identifiant" name="identifiant" class="identifiant">
                        <span id="error" class="errors"><?php viewError($errors, 'identifiant');  ?></span>
                    </div>
                    <div class="bot">
                        <label for="email">Email :</label>
                        <input type="email" id="email" name="email" class="email">
                        <span class="errors" id="error"><?php viewError($errors, 'email') ?></span>
                    </div>
                </div>
                <div class="second-container">
                    <div class="left">
                        <label for="mdp">Mot de passe :</label>
                        <input type="password" name="mdp" id="mdp" class="mdp">
                        <span id="error" class="errors"><?php viewError($errors, 'mdp');  ?></span>
                    </div>
                    <div class="right">
                        <label for="mdp-confirmation">Confirmation</label>
                        <input type="password" name="mdp-confirmation" id="mdp-confirmation" class="mdp-confirmation">
                        <span id="error" class="errors"><?php viewError($errors, 'mdp-confirmation');  ?></span>
                    </div>
                </div>
                <div class="third-container">
                    <input type="checkbox" name="checkbox">
                    <p>Je déclare avoir pris connaissance des conditions générales d'utilisation du site internet et les accepter.</p>
                    <span id="error" class="errors"><?php viewError($errors, 'checkbox');  ?></span>
                </div>
                <div class="captcha-container">
                    <div name="captcha" id="recaptcha_id" class="g-recaptcha" data-sitekey="6Le9p4QjAAAAAN8pjYnVn-88EmoCOnLhqc2HGWDf"></div>
                    <span class="errors" id="error"><?php viewError($errors, 'captcha') ?></span>

                </div>
                <span class="errors" id="error"><?php viewError($errors, 'err-all') ?></span>
                <input type="submit" value="Inscription" name="submit" id="submitted" class="submitted">
            </div>
        </div>
    </section>
</form>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<?php



get_footer() ?>