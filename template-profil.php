<?php
/* Template Name: Profil */
global $web;
global $metas;

get_header();

?>


<div id="profil-main">
    <div id="sidebar_left">
        <aside id="secondary" class="widget-area">
            <div class="onglets">
                <section id="sidebar">
                    <?php
                    $current_user = wp_get_current_user();
                    $first_name = $current_user->user_firstname;
                    $last_name = $current_user->user_lastname;
                    $role = $current_user->roles[0];
                    $nickname = $current_user->nickname;
                    $profile_image = get_avatar_url($current_user->ID);
                    ?>

                    <div class="idendity">
                        <?php if (!empty($profile_image)) { ?>
                            <img id="profilpic" src="<?php echo $profile_image; ?>" alt="Profile image">
                        <?php } ?>
                        <h2><?php echo $first_name . " " . $last_name; ?></h2>
                        <h3><?php echo $role; ?></h3>
                        <h4><?php echo $nickname; ?></h4>
                    </div>
                    <div class="container_all">
                        <nav class="container-onglets">
                            <ul>
                                <li class="onglets active" id="onglet1" data-anim="1">Resume</li>
                                <li class="onglets" id="onglet2" data-anim="2">Mes Informations</li>
                                <li class="onglets" id="onglet3" data-anim="3">Mes CV</li>
                                <li class="onglets" id="onglet4" data-anim="4">Réglages</li>
                            </ul>
                        </nav>
                    </div>
                    <div class="help">
                        <div class="style_help">
                            <div class="txt_help">
                                <h2>Besoin d’aide ?</h2>
                                <h3>Visitez notre FAQ !</h3>
                            </div>
                            <div class="logo_help">
                                <a href="<?= path('/'); ?>"><img src="<?= asset('/image_profil_aide.png'); ?>" alt="logo"></a>
                            </div>
                            <div class="txt_button">
                                <p type="button">Clique ici</p>
                            </div>
                            <?php if (!empty($metas['img_help'][0])) {
                                echo getImageById($metas['img_help'][0], 'img_help', 'alt', 'img_help');
                            } ?>
                        </div>
                    </div>
                </section>
            </div>

        </aside><!-- #secondary -->

    </div>
    <div id="main-content">
        <div id="divonglet1">
            <section id="resume">
                <div class="wrap">
                    <div class="container">
                        <div class="profil">
                            <div class="presentation">
                                <div class="present">
                                    <img src="" class="pic_profil" alt="">
                                    <h2 class="name"><?php echo $first_name . " " . $last_name; ?></h2>
                                    <h3 class="status"><?php echo $role; ?></h3>
                                    <div class="stat">
                                        <h2>Profil compléter<span>60%</span></h2>
                                        <p class="block"></p>
                                        <h2>Votre cv à été consulté<span>251 vues</span></h2>
                                    </div>
                                </div>
                                <div class="intro">
                                    <h2 class="title">A propos</h2>
                                    <p class="txt">Je suis un développeur passionné par l'automatisation et la mise en production efficace des applications. En tant que DevOps, je suis à la fois un développeur et un administrateur système, ce qui me permet de comprendre les défis techniques auxquels les équipes sont confrontées.
                                        Je suis spécialisé dans la mise en place d'une infrastructure résiliente et scalable, ainsi que dans l'intégration continue et la livraison continue. Je suis également expérimenté dans la mise en place de processus de tests automatisés pour garantir la qualité du code.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="competence">
                            <h2 class="title">HardSkill</h2>
                            <div id="hardskill"></div>
                            <line></line>
                        </div>
                        <div class="competence">
                            <h2 class="title">Softskill</h2>
                            <div id="competence"></div>
                            <line></line>
                        </div>
                    </div>
                    <div class="boxs boxlarge">
                        <div class="competence">
                            <h2 class="title">Expérience Professionel</h2>
                            <div id="experience"></div>
                            <line></line>
                        </div>
                    </div>
                    <div class="boxs boxlarge">
                        <div class="competence">

                            <h2 class="title">Formation</h2>
                            <div id="resumeformation"></div>
                            <line></line>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="competence">
                            <h2 class="title">Langue</h2>
                            <div id="Langue"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div id="divonglet2"><?php




                                if (is_user_logged_in()) {
                                    $current_user = wp_get_current_user();
                                    $user_id = $current_user->ID;

                                    // Récupération des métadonnées de l'utilisateur
                                    $user_first_name = get_user_meta($user_id, 'first_name', true);
                                    $user_last_name = get_user_meta($user_id, 'last_name', true);
                                    $user_address = get_user_meta($user_id, 'adresse', true);
                                    $user_city = get_user_meta($user_id, 'ville', true);
                                    $user_zip_code = get_user_meta($user_id, 'codepostal', true);
                                    $user_birthdate = get_user_meta($user_id, 'naissance-date', true);
                                    $user_gender = get_user_meta($user_id, 'genre', true);
                                }

                                if (isset($_POST['submit'])) {
                                    // Mettre à jour les métadonnées de l'utilisateur
                                    update_user_meta($user_id, 'first_name', sanitize_text_field($_POST['prenom']));
                                    update_user_meta($user_id, 'last_name', sanitize_text_field($_POST['nom']));
                                    update_user_meta($user_id, 'adresse', sanitize_text_field($_POST['adresse']));
                                    update_user_meta($user_id, 'ville', sanitize_text_field($_POST['ville']));
                                    update_user_meta($user_id, 'codepostal', sanitize_text_field($_POST['code_postal']));
                                    update_user_meta($user_id, 'naissance-date', sanitize_text_field($_POST['date']));
                                    update_user_meta($user_id, 'genre', sanitize_text_field($_POST['genre']));
                                }

                                ?>
            <!--FORMULAIRE INFO -->
            <section id="form-info">
                <div id="dropdown">
                    <div id="title-info">
                        <h2>Informations</h2>
                        <span id="close-info">+</span>
                    </div>
                    <form method="post" name="form_id" action="#" class="opened" id="form">
                        <div class="form_box1">
                            <div class="left">
                                <label for="text">Nom :</label>
                                <input type="text" value="<?php echo $user_last_name; ?>" name="nom" placeholder="<?php echo $user_last_name; ?>">
                                <span></span>
                            </div>
                            <div class="right">
                                <label for="">Prénom :</label>
                                <input type="text" value="<?php echo $user_first_name; ?>" name="prenom" placeholder="<?php echo $user_first_name; ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="box-mid">
                            <label for="">Adresse :</label>
                            <input type="text" value="<?php echo $user_address; ?>" name="adresse" placeholder="<?php echo $user_address; ?>">
                            <span></span>
                        </div>
                        <div class="form_box2">
                            <div class="left">
                                <label for="">Ville :</label>
                                <input type="text" value="<?php echo $user_city; ?>" placeholder="<?php echo $user_city; ?>" name="ville">
                                <span></span>
                                <label for="">Date de naissance :</label>
                                <input type="date" value="<?php echo $user_birthdate; ?>" placeholder="<?php echo $user_birthdate; ?>" name="date">
                                <span></span>
                            </div>
                            <div class="right">
                                <label for="">Code Postal :</label>
                                <input type="text" value="<?php echo $user_zip_code; ?>" placeholder="<?php echo $user_zip_code; ?>" name="code_postal">
                                <span></span>

                                <label>Genre :</label>
                                <select name="genre" id="genre" value="<?php echo $user_gender; ?>">
                                    <option value="">--Genre--</option>
                                    <option value="Homme">Homme</option>
                                    <option value="Femme">Femme</option>
                                    <option value="Autre">Autre</option>
                                </select>
                                <span></span>
                            </div>
                        </div>
                        <div class="form_button">
                            <input type="button" class="btn-annuler" value="Annuler">
                            <input type="submit" name="submit" class="btn-sauvegarder" value="Sauvegarder">
                        </div>
                    </form>
            </section>

            <!--FORMULAIRE SoftSkill -->
            <section id="form-softskill">
                <div id="title-softskill">
                    <h2>Softskills</h2>
                    <span id="close-softskill">+</span>
                </div>
                <div id="div-softskill">
                    <div id="todo-form">
                        <ul id="todo-list"></ul>
                        <form method="post" class="add-group">
                            <select id="task-name" name="softskill">
                                <option value="COMMUNICATION">COMMUNICATION</option>
                                <option value="TRAVAIL EN ÉQUIPE">TRAVAIL EN ÉQUIPE</option>
                                <option value="FLEXIBILITÉ">FLEXIBILITÉ</option>
                                <option value="ORGANISATION">ORGANISATION</option>
                                <option value="CRÉATIVITÉ">CRÉATIVITÉ</option>
                                <option value="LEADERSHIP">LEADERSHIP</option>
                                <option value="PROACTIVITÉ">PROACTIVITÉ</option>
                                <option value="ADAPTATION">ADAPTATION</option>
                                <option value="RÉSOLUTION DE PROBLÈMES">RÉSOLUTION DE PROBLÈMES</option>
                                <option value="GESTION DU STRESS">GESTION DU STRESS</option>
                                <option value="NÉGOCIATION">NÉGOCIATION</option>
                                <option value="GESTION DU TEMPS">GESTION DU TEMPS</option>
                                <option value="EMPATHIE">EMPATHIE</option>
                                <option value="LEARNING AGILITY">LEARNING AGILITY</option>
                                <option value="ENTREPRENEURIAT">ENTREPRENEURIAT</option>
                                <option value="GESTION DES CONFLITS">GESTION DES CONFLITS</option>
                                <option value="INITIATIVE">INITIATIVE</option>
                            </select>
                            <input type="submit" name="addsoftskill" class="btn-sauvegarder" id="submit_form2" value="Sauvegarder">
                        </form>
                    </div>
                </div>
            </section>

            <!--FORMULAIRE HardSkill -->
            <section id="form-hardskill2">
                <div id="title-hardskill2">
                    <h2>Hardskills</h2>
                    <span id="close-hardskill2">+</span>
                </div>
                <div id="div-hardskill2">
                    <div id="todo-form2">
                        <ul id="todo-list2">
                        </ul>
                        <form method="post" class="add-group2">
                            <select id="task-name2" name="hardskill">
                                <option value="AJAX">AJAX</option>
                                <option value="Assembly">Assembly</option>
                                <option value="C">C</option>
                                <option value="C++">C++</option>
                                <option value="C#">C#</option>
                                <option value="CSS">CSS</option>
                                <option value="Go">Go</option>
                                <option value="HTML">HTML</option>
                                <option value="Java">Java</option>
                                <option value="JavaScript">JavaScript</option>
                                <option value="Julia">Julia</option>
                                <option value="Kotlin">Kotlin</option>
                                <option value="Lisp">Lisp</option>
                                <option value="Lua">Lua</option>
                                <option value="Matlab">Matlab</option>
                                <option value="Node.js">Node.js</option>
                                <option value="Objective-C">Objective-C</option>
                                <option value="Pascal">Pascal</option>
                                <option value="Perl">Perl</option>
                                <option value="PHP">PHP</option>
                                <option value="Python">Python</option>
                                <option value="R">R</option>
                                <option value="Ruby">Ruby</option>
                                <option value="Rust">Rust</option>
                                <option value="Scala">Scala</option>
                                <option value="SQL">SQL</option>
                                <option value="Swift">Swift</option>
                                <option value="TypeScript">TypeScript</option>
                                <option value="Visual Basic">Visual Basic</option>
                                <option value="XML">XML</option>
                            </select>
                            <select id="rating2" name="rating2">
                                <option value="">Niveau</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <input type="submit" name="addhardSkill" class="btn-sauvegarder" id="submit_form3" value="Sauvegarder">
                        </form>
                    </div>
                </div>
            </section>

            <!--FORMULAIRE Diplome -->
            <section id="form-diplome">
                <div id="title-diplome">
                    <h2>Formation</h2>
                    <span id="close-diplome">+</span>
                </div>
                <div id="div-diplome">
                    <div id="todo-form3">
                        <ul id="todo-list3">
                        </ul>
                        <form method="post" class="add-group3">
                            <select id="task-name3" name="diplome">
                                <option value="">Sélectionnez un diplôme</option>
                                <option value="Brevet des Collèges">Brevet des Collèges</option>
                                <option value="Baccalauréat">Baccalauréat</option>
                                <option value="Diplôme Universitaire de Technologie">Diplôme Universitaire de Technologie</option>
                                <option value="Licence">Licence</option>
                                <option value="Maîtrise">Maîtrise</option>
                                <option value="Diplôme d'Ingénieur">Diplôme d'Ingénieur</option>
                                <option value="Doctorat">Doctorat</option>
                            </select>

                            <input type="text" id="diplome_name" name="diplome_name" placeholder="Spécialité">
                            <input type="submit" name="adddiplome" class="btn-sauvegarder" id="submit_form3" value="Sauvegarder">
                        </form>
                    </div>
                </div>
            </section>

            <!--FORMULAIRE Experience Pro -->

            <section id="form-experience">
                <div id="title-experience">
                    <h2>Experiences Professionnelles</h2>
                    <span id="close-experience">+</span>
                </div>
                <div id="div-experience">
                    <div id="experience-form">
                        <ul id="experience-list">
                        </ul>
                        <form method="post" class="add-group4">
                            <div class="form-group">
                                <label for="date_in">Date de début</label>
                                <input type="date" id="date_in" name="date_in" placeholder="Date de début">
                            </div>
                            <div class="form-group">
                                <label for="date_out">Date de fin</label>
                                <input type="date" id="date_out" name="date_out" placeholder="Date de fin">
                            </div>
                            <div class="form-group">
                                <label for="job">Emplois</label>
                                <input type="text" id="job" name="job" placeholder="Emplois">
                            </div>
                            <div class="form-group">
                                <label for="where">Lieux</label>
                                <input type="text" id="where" name="place" placeholder="Lieux">
                            </div>
                            <input type="submit" name="addexperience" class="btn-sauvegarder" id="submit_form4" value="Sauvegarder">
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <div id="divonglet3">
            <a href="CVPDF"><button>Télécharger</button></a>
            <div id="pdf"></div>
        </div>
        <div id="divonglet4"></div>

    </div>
</div>

<?php get_footer() ?>