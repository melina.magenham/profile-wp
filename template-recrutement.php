<?php
/* Template Name: Rectrutement */
get_header(); ?>

<div class="wrap2">
    <div id="profil-main2">
        <div id="sidebar_left">
            <?php
            global $web;
            global $metas;
            ?>

            <aside id="secondary" class="widget-area">
                <div class="onglets">
                    <section id="sidebar">
                        <?php
                        $current_user = wp_get_current_user();
                        $first_name = $current_user->user_firstname;
                        $last_name = $current_user->user_lastname;
                        $role = $current_user->roles[0];
                        $nickname = $current_user->nickname;
                        $profile_image = get_avatar_url($current_user->ID);
                        ?>

                        <div class="idendity">
                            <?php if (!empty($profile_image)) { ?>
                                <img id="profilpic" src="<?php echo $profile_image; ?>" alt="Profile image">
                            <?php } ?>
                            <h2><?php echo $first_name . " " . $last_name; ?></h2>
                            <h3><?php echo $role; ?></h3>
                            <h4><?php echo $nickname; ?></h4>
                        </div>
                        <div class="container_all">
                            <nav class="container-onglets">
                                <ul>
                                    <li class="onglets active" id="onglet1" data-anim="1">Les CV</li>
                                    <li class="onglets" id="onglet" data-anim=""></li>
                                    <li class="onglets" id="onglet3" data-anim="3"></li>
                                    <li class="onglets" id="onglet4" data-anim="4"></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="help">
                            <div class="style_help">
                                <div class="txt_help">
                                    <h>Besoin d’aide ?</h>
                                    <h3>Visitez notre FAQ !</h3>

                                </div>
                                <div class="logo_help">
                                    <a href="<?= path('/'); ?>"><img src="<?= asset('/image_profil_aide.png'); ?>" alt="logo"></a>
                                </div>
                                <div class="txt_button">
                                    <p type="button">Clique ici</p>
                                </div>
                                <?php if (!empty($metas['img_help'][0])) {
                                    echo getImageById($metas['img_help'][0], 'img_help', 'alt', 'img_help');
                                } ?>
                            </div>
                        </div>
                    </section>
                </div>

            </aside><!-- #secondary -->
        </div>

        <section id="listing_profiles">
            <h1>Voir les candidats</h1>
            <ul>
                <?php
                $arg4 = [
                    'role' => 'candidat'
                ];
                $users = get_users($arg4);
                foreach ($users as $user) {
                    echo '
                    <li>
                        <a href="single-user?id=' . $user->ID . '">
                            <div class="lprf_left">
                                <div class="lprfl_top">
                                    ' . get_avatar($user->ID) . '
                                </div>
                                <div class="lprfl_bottom">
                                    <h2>' . $user->display_name . '</h2>
                                    <p>Dev ops Senior</p>
                                </div>
                            </div>
                        </a>
                    </li>';
                } ?>
            </ul>
        </section>
    </div>
</div>

<?php get_footer() ?>